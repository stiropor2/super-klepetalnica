var seznamNadimkovUporabnika = [];
var seznamPredNadimkom = [];
var stevecIzpisanih = []; //da ne izpise veckrat istega uporabnika na kanalu

/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var triggerSporocilo = "&#9756;";
  var pogojKrcanje = sporocilo.indexOf(triggerSporocilo) > -1;
  if (pogojKrcanje) {
    return divElementHtmlTekst(sporocilo);  
  } else {
            var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
            if (jeSmesko) {
              sporocilo = 
                sporocilo.split("<").join("&lt;")
                         .split(">").join("&gt;")
                         .split("&lt;img").join("<img")
                         .split("png' /&gt;").join("png' />");
              return divElementHtmlTekst(sporocilo);
            } else {
              return $('<div style="font-weight: bold"></div>').text(sporocilo);  
            }
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno2 besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    var besedica = sporocilo.split(' ');
        if (besedica[0].toLowerCase() == '/barva') {
          ukazek = besedica[1].toLowerCase();
         document.getElementById("kanal").style.backgroundColor = ukazek;
         document.getElementById("sporocila").style.backgroundColor = ukazek;
          //console.log(ukazek);
        } else {
        
                  sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
                  if (sistemskoSporocilo) {
                    $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
                  }
        }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    
    
    
    
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
   //var hranilecSporocila = sporocilo;
   var seznamBesedic = sporocilo.split(' ');
   var trenutniInput;
     for (var i=0; i< seznamBesedic.length; i++) {
      if (seznamBesedic[i].match(/(https?:\/\/.*\.(?:png|jpg|gif|jpeg))/i)) {
        trenutniInput = $('<br /><img src="' + seznamBesedic[i] + '" style="width:200px;padding-left:20px">');
        $('#sporocila').append(trenutniInput);
      }
    }
  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  	socket.on('vzdevekSpremembaOdgovor', function(rezultat) {	
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var detectSpremenjeno = sporocilo.besedilo.split(' ');
    //console.log(detectSpremenjeno[0]);
    var spremenjenoSporocilo = sporocilo.besedilo;
    for (var i=0; i< seznamPredNadimkom.length; i++) {
   
      if (detectSpremenjeno[0].replace(/:/g, "") == seznamPredNadimkom[i]) {
        detectSpremenjeno[0] = seznamNadimkovUporabnika[i] + ' (' + detectSpremenjeno[0].replace(/:/g, "") + ')' + ': ';
        spremenjenoSporocilo = detectSpremenjeno.join(' ');
        break;
      }
    }

    var novElement = divElementEnostavniTekst(spremenjenoSporocilo);
    $('#sporocila').append(novElement);
   
   //var hranilecSporocila = sporocilo;
   //var seznamBesedic = sporocilo.split(' ');
   var seznamBesedic = sporocilo.besedilo.split(' ');
   //console.log(seznamBesedic);
   var trenutniInput;
     for (var i=0; i< seznamBesedic.length; i++) {
      if (seznamBesedic[i].match(/(https?:\/\/.*\.(?:png|jpg|gif|jpeg))/i)) {
        //console.log("slikca je v tekstu! " + seznamBesedic[i]);
        trenutniInput = $('<br /><img src="' + seznamBesedic[i] + '" style="width:200px;padding-left:20px">');
        $('#sporocila').append(trenutniInput);
        //console.log(seznamZaPrikaz[stevecSlik]);
        //console.log("Beseda " + i + " tekst; " + seznamBesedic[i]);
      }
    }
    
    
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    var displayNadimek;
    for (var i=0; i < uporabniki.length; i++) {
      if (seznamNadimkovUporabnika.length > 0) {
        for (var i2=0; i2 < seznamNadimkovUporabnika.length; i2++) {
          //console.log("tralala");
          if (seznamPredNadimkom[i2] == uporabniki[i]) {
            displayNadimek = seznamNadimkovUporabnika[i2] + ' ' + '(' + uporabniki[i] + ')';

              $('#seznam-uporabnikov').append(divElementEnostavniTekst(displayNadimek));
            break;
          } else {
            if (seznamNadimkovUporabnika.length == (i2+1)) {
              //console.log("tole se je zgodil");
                $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
              break;
            }
          }
        }
      } else {
       $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }

    //console.log("hello!");
      $('#seznam-uporabnikov div').click(function() {
    //console.log("its me!");
      klepetApp.procesirajUkaz('/zasebno ' + '"' + $(this).text() + '"' + ' "' + '&#9756;' + '"');
      $('#poslji-sporocilo').focus();
    });
  });
  
  // pridobi
  socket.on('dodajanjeNadimka', function (vnos) {
    //console.log("it happens");
    if (seznamNadimkovUporabnika.length != 0) {
      for (var i=0; i< seznamNadimkovUporabnika.length; i++) {
        if (seznamPredNadimkom[i] == vnos.predNadimkom) {
          seznamNadimkovUporabnika[i] = vnos.nadimek;
        } else {
          if (i == (seznamNadimkovUporabnika.length - 1)) {
            seznamNadimkovUporabnika.push(vnos.nadimek);
            seznamPredNadimkom.push(vnos.predNadimkom);
          }
        }
      }
    } else {
      seznamNadimkovUporabnika.push(vnos.nadimek);
      seznamPredNadimkom.push(vnos.predNadimkom);
    }

      //console.log("stara imena: " + seznamPredNadimkom + "Nova imena " + seznamNadimkovUporabnika);
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
